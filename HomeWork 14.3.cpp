#include <string>
#include <iostream>

using namespace std;

int main()
{
    cout << "Enter some text: \n";
    string text;
    
    getline(cin, text);

    cout << "\n" << text << endl << "Line length = " << text.length() << endl << text[0] << endl << text.back();

}

